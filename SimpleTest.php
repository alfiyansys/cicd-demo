<?php

use PHPUnit\Framework\TestCase;

require_once "Konversi.php";

class SimpleTest extends TestCase{
	public function testConvert(){
		$conv = new Konversi();

		$this->assertEquals(212, $conv->convert("ctf",100));
		$this->assertEquals(32, $conv->convert("ctf",0));

		$this->assertEquals(0, $conv->convert("ftc",32));
		$this->assertEquals(100, $conv->convert("ftc",212));

		$this->assertEquals(100, $conv->convert("rtc",80));
		$this->assertEquals(50, $conv->convert("rtc",40));
	}
}
