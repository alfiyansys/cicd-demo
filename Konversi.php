<?php

Class Konversi{
    public function convert($direction, $originValue){
        $targetValue = 0;
        switch($direction){
            case "ctf":
                $targetValue = ($originValue * 9/5) + 32;
                break;
            case "ftc":
                $targetValue = ($originValue - 32) * 5/9;
                break;
            case "rtc":
                $targetValue = $originValue * 5/7;
                break;
        }

        return $targetValue;
    }
}
